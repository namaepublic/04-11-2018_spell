﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "EnemyModel", fileName = "Enemy")]
public class EnemyModel : ScriptableObject
{
	public Sprite sprite;
}
