﻿using System;
using UnityEngine;

[CreateAssetMenu(menuName = "SpellModel", fileName = "Spell")]
public class SpellModel : ScriptableObject
{
    public string name;
    public Sprite icon;

    public bool CompareName(string value)
    {
        return string.Compare(name, value, StringComparison.OrdinalIgnoreCase) == 0;
    }
}