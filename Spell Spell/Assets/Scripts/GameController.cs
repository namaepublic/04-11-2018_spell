﻿using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

public class GameController : MonoBehaviour
{
    [SerializeField] private SpellModel[] _spellModels;
    [SerializeField] private EnemyModel[] _enemyModels;

    [SerializeField] private GameObject _menuCanvas;
    [SerializeField] private TMP_InputField _playInput;

    [SerializeField] private TextMeshProUGUI _scoreText;
    [SerializeField] private TMP_InputField _spellInput;
    [SerializeField] private Image _enemyBar, _playerBar;
    [SerializeField] private Image _spellIcon;
    [SerializeField] private SpriteRenderer _enemySprite;

    [SerializeField] private int _playerMaxHp = 10;
    [SerializeField] private float _damageCooldown = 1;

    private SpellModel _spell;
    private int _enemyHp, _playerHp;
    private int _level;
    private float _currentTime;
    private bool _init;

    private SpellModel Spell
    {
        get { return _spell; }
        set
        {
            _spell = value;
            _spellIcon.sprite = _spell.icon;
        }
    }

    private int EnemyHp
    {
        get { return _enemyHp; }
        set
        {
            _enemyHp = value;
            _enemyBar.fillAmount = (float) _enemyHp / (_level + 1);
            if (_enemyHp <= 0)
                NextLevel();
        }
    }

    private int PlayerHp
    {
        get { return _playerHp; }
        set
        {
            _playerHp = value;
            _playerBar.fillAmount = (float) _playerHp / _playerMaxHp;
            if (_playerHp <= 0)
                GameOver();
        }
    }

    private int Level
    {
        get { return _level; }
        set
        {
            _level = value;
            _scoreText.text = _level.ToString();
        }
    }

#if UNITY_EDITOR
    private void OnValidate()
    {
        _spellModels = FindAssetsByType<SpellModel>().ToArray();
        _enemyModels = FindAssetsByType<EnemyModel>().ToArray();
    }
#endif
    
    private void Start()
    {
        _init = false;
        _menuCanvas.SetActive(true);
    }

    private void Init()
    {
        _menuCanvas.SetActive(false);
        _init = true;
        _level = -1;
        NextLevel();
    }

    private void Update()
    {
        if (!_init)
        {
            _playInput.Select();
            _playInput.ActivateInputField();
            return;
        }

        if (_currentTime < _damageCooldown)
            _currentTime += Time.deltaTime;
        else
            TakeDamage();
        _spellInput.Select();
        _spellInput.ActivateInputField();

        if (Input.GetKeyDown(KeyCode.Return))
            OnClickLaunch();
        ;
    }

    private void TakeDamage()
    {
        _currentTime = 0;
        PlayerHp--;
    }

    private void NextLevel()
    {
        Level++;
        _currentTime = 0;
        PlayerHp = _playerMaxHp;
        EnemyHp = _level + 1;
        Spell = _spellModels[Random.Range(0, _spellModels.Length)];
        _enemySprite.sprite = _enemyModels[Random.Range(0, _enemyModels.Length)].sprite;
    }

    private void GameOver()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void OnClickLaunch()
    {
        if (_spell.CompareName(_spellInput.text))
        {
            EnemyHp--;
            Spell = _spellModels[Random.Range(0, _spellModels.Length)];
        }

        _spellInput.text = "";
    }

    public void OnPlayInputChanged(string value)
    {
        if (string.Compare(value, "quit", StringComparison.OrdinalIgnoreCase) == 0)
        {
            Quit();
        }

        if (string.Compare(value, "play", StringComparison.OrdinalIgnoreCase) == 0)
        {
            Init();
        }
    }

    private void Quit()
    {
#if UNITY_EDITOR
        EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

#if UNITY_EDITOR
    public static IEnumerable<T> FindAssetsByType<T>() where T : Object
    {
        return AssetDatabase.FindAssets(string.Format("t:{0}", typeof(T))).Select(AssetDatabase.GUIDToAssetPath)
            .Select(AssetDatabase.LoadAssetAtPath<T>).Where(asset => asset != null);
    }
#endif
}